msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Calligraphy\n"
"POT-Creation-Date: 2021-10-11 17:29+0200\n"
"PO-Revision-Date: 2010-08-24 10:15+0700\n"
"Last-Translator: @CameliaGirls Translation Team <cameliagirls@googlegroups."
"com>\n"
"Language-Team: Indonesian <id@li.org>\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr "pengakuan untuk penerjemah < >, "

#. (itstool) path: articleinfo/title
#: tutorial-calligraphy.xml:6
msgid "Calligraphy"
msgstr "Kaligrafi"

#. (itstool) path: articleinfo/subtitle
#: tutorial-calligraphy.xml:7
msgid "Tutorial"
msgstr ""

#. (itstool) path: abstract/para
#: tutorial-calligraphy.xml:11
msgid ""
"One of the many great tools available in Inkscape is the Calligraphy tool. "
"This tutorial will help you become acquainted with how that tool works, as "
"well as demonstrate some basic techniques of the art of Calligraphy."
msgstr ""
"Salah satu dari banyak alat hebat yang disediakan dalam Inkscape adalah "
"Calligraphy (Kaligrafi) tool. Tutorial kali ini akan membantu anda terbiasa "
"dengan bagaimana tool tersebut bekerja, sekaligus mendemonstrasikan beberapa "
"tehnik dasar dari seni kaligrafi. "

#. (itstool) path: abstract/para
#: tutorial-calligraphy.xml:15
#, fuzzy
msgid ""
"Use <keycombo><keycap function=\"control\">Ctrl</keycap><keycap>arrows</"
"keycap></keycombo>, <mousebutton role=\"mouse-wheel\">mouse wheel</"
"mousebutton>, or <mousebutton role=\"middle-button-drag\">middle button "
"drag</mousebutton> to scroll the page down. For basics of object creation, "
"selection, and transformation, see the Basic tutorial in "
"<menuchoice><guimenu>Help</guimenu><guimenuitem>Tutorials</guimenuitem></"
"menuchoice>."
msgstr ""
"Gunakan <keycap>Ctrl+panah</keycap>, <keycap>Roda tetikus</keycap>, atau "
"<keycap>seret dengan tombol tengah</keycap> untuk menggulung halaman ke "
"bawah. Untuk dasar membuat obyek, seleksi, dan transformasi, lihatlah "
"tutorial Dasar pada <command>Help &gt; Tutorials</command>."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:23
msgid "History and Styles"
msgstr "Sejarah dan Gaya"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:24
msgid ""
"Going by the dictionary definition, <firstterm>calligraphy</firstterm> means "
"“beautiful writing” or “fair or elegant penmanship”. Essentially, "
"calligraphy is the art of making beautiful or elegant handwriting. It may "
"sound intimidating, but with a little practice, anyone can master the basics "
"of this art."
msgstr ""
"Berdasarkan penjelasan kamus, <firstterm>calligraphy</firstterm> alias "
"Kaligrafi artinya ”tulisan indah” atau ”cara menggunakan pena yang baik dan "
"elegan”. Intinya, kaligrafi adalah seni membuat tulisan tangan yang indah "
"dan elegan. Mungkin terdengar mengintimidasi, tapi dengan sedikit latiah, "
"semua bisa menguasai dasar dari seni ini."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:29
#, fuzzy
msgid ""
"Up until roughly 1440 AD, before the printing press was around, calligraphy "
"was the way books and other publications were made. A scribe had to "
"handwrite every individual copy of every book or publication. Perhaps the "
"most common place where the average person will run across calligraphy today "
"is on wedding invitations. There are three main styles of calligraphy:"
msgstr ""
"Bentuk paling dasar dari kaligrafi ada sejak jaman manusia gua menulis di "
"dinding. Sampai sekitar 1440 masehi, sebelum percetakan ada, kaligrafi "
"adalah cara bagaimana buku dan publikasi lainnya dibuat. Gaya huruf yang "
"digunakan masa itu termasuk Rustic, Carolingian, Blackletter, dan lainnya. "
"Mungkin yang paling mudah ditemukan sekarang adalah pada undangan pernikahan."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:32
msgid "There are three main styles of calligraphy:"
msgstr "Terdapat tiga gaya utama dari kaligrafi:"

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:37
msgid ""
"Western or Roman: The preserved texts are mainly written with a quill and "
"ink onto materials such as parchment or vellum."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:42
msgid ""
"Arabic, including other languages in Arabic script, executed with a reed pen "
"and ink on paper. (This style has produced by far the largest bulk of "
"preserved texts before 1440.)"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:47
#, fuzzy
msgid "Chinese or Oriental, executed with a brush."
msgstr "Cina atau Oriental"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:52
msgid ""
"The three styles have influenced each other on different occasions. Western "
"lettering styles used throughout the ages include Rustic, Carolingian, "
"Blackletter, etc."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:55
#, fuzzy
msgid ""
"One great advantage that we have over the scribes of the past is the "
"<guimenuitem>Undo</guimenuitem> command: If you make a mistake, the entire "
"page is not ruined. Inkscape's Calligraphy tool also enables some techniques "
"which would not be possible with a traditional pen-and-ink."
msgstr ""
"Salah satu keuntungan kita dibanding masa lalu adalah adanya perintah "
"<command>Undo</command>: Jika anda salah, seluruh halaman tidak ikut rusak. "
"Calligraphy tool juga membuat kita bisa melakukan beberapa tehnik yang tidak "
"mungkin dilakukan dengan pena dan tinta."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:62
msgid "Hardware"
msgstr "Perangkat Keras"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:63
msgid ""
"You will get the best results if you use a <firstterm>tablet and pen</"
"firstterm> (e.g. Wacom). Thanks to the flexibility of our tool, even those "
"with only a mouse can do some fairly intricate calligraphy, though there "
"will be some difficulty producing fast sweeping strokes."
msgstr ""
"Anda bisa mendapatkan hasil terbaik jika anda menggunakan <firstterm>tablet "
"and pen</firstterm> (mis. Wacom). Terimakasih terhadap fleksibilitas tool "
"kami, bahkan dengan tetikus saja, anda bisa menghasilkan kaligrafi yang "
"cukup indah, walaupun akan sedikit susah jika ingin menghasilkan garis "
"sapuan secara cepat."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:68
msgid ""
"Inkscape is able to utilize <firstterm>pressure sensitivity</firstterm> and "
"<firstterm> tilt sensitivity</firstterm> of a tablet pen that supports these "
"features. The sensitivity functions are disabled by default because they "
"require configuration. Also, keep in mind that calligraphy with a quill or "
"pen with nib are also not very sensitive to pressure, unlike a brush."
msgstr ""
"Inkscape mampu menggunakan <firstterm>pressure sensitivity</firstterm> dan "
"<firstterm>tilt sensitivity</firstterm> dari sebuah tablet pen yang "
"mendukung fitur tersebut. Fungsi/fitur tersebut dinonaktifkan secara standar "
"karena mereka membutuhkan konfigurasi lebih lanjut. Juga, selalu ingat bahwa "
"kaligrafi dengan pena bulu atau pena biasa tidak terlalu sensitif terhadap "
"tekanan (pressure sensitivty), tidak seperti kuas."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:74
#, fuzzy
msgid ""
"If you have a tablet and would like to utilize the sensitivity features, you "
"will need to configure your device. This configuration will only need to be "
"performed once and the settings are saved. To enable this support you must "
"have the tablet plugged in prior to starting Inkscape and then proceed to "
"open the <guimenuitem>Input Devices...</guimenuitem> dialog through the "
"<guimenu>Edit</guimenu> menu. With this dialog open you can choose the "
"preferred device and settings for your tablet pen. Lastly, after choosing "
"those settings, switch to the Calligraphy tool and toggle the toolbar "
"buttons for pressure and tilt. From now on, Inkscape will remember those "
"settings on startup."
msgstr ""
"Jika anda memiliki tablet dan ingin menggunakan fitur tersebut, anda perlu "
"mengkonfigurasi alat anda. Konfigurasi ini hanya dilakukan sekali dan "
"penataannya disimpan. Untuk mengaktifkannya, anda harus memasang tablet "
"tersebut sebelum menjalankan Inkscape kemudian membuka dialog "
"<firstterm>Input Devices...</firstterm> lewat menu <emphasis>File</"
"emphasis>. Setelah dialog ini dibuka, anda bisa memilih alat dan tata untuk "
"pen tablet anda. Terakhir, setelah memilih penataannya, berpindahlah ke "
"Calligraphy tool dan nyalakan tombol pressure and tilt. Seterusnya, Inkscape "
"akan selalu mengingat penataan tersebut setiap memulai."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:83
msgid ""
"The Inkscape calligraphy pen can be sensitive to the <firstterm>velocity</"
"firstterm> of the stroke (see “Thinning” below), so if you are using a "
"mouse, you'll probably want to zero this parameter."
msgstr ""
"Pena kaligrafi inkscape bisa diatur untuk sensitif terhadap "
"<firstterm>velocity</firstterm> dari sapuan (lihat “thinning-perampingan” "
"dibawah), jadi, jika anda menggunakan tetikus, kemungkinan anda ingin "
"menggunakan nol untuk parameter ini."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:89
msgid "Calligraphy Tool Options"
msgstr "Opsi Calligraphy Tool"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:90
#, fuzzy
msgid ""
"Switch to the Calligraphy tool by pressing <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>F6</keycap></keycombo>, pressing the "
"<keycap>C</keycap> key, or by clicking on its toolbar button. On the top "
"toolbar, you will notice there are 8 options: <guilabel>Width</guilabel> "
"&amp; <guilabel>Thinning</guilabel>; <guilabel>Angle</guilabel> &amp; "
"<guilabel>Fixation</guilabel>; <guilabel>Caps</guilabel>; <guilabel>Tremor</"
"guilabel>, <guilabel>Wiggle</guilabel> &amp; <guilabel>Mass</guilabel>. "
"There are also two buttons to toggle tablet Pressure and Tilt sensitivity on "
"and off (for drawing tablets)."
msgstr ""
"Berpindahlah ke Calligraphy Tool dengan menekan <keycap>Ctrl+F6</keycap>, "
"tombol <keycap>C</keycap>, atau dengan klik pada tombol toolbarnya. Pada "
"toolbar atas, anda bisa melihat bahwa terdapat 8 opsi/pilihan: Width &amp; "
"Thinning; Angle &amp; Fixation; Caps; Tremor, Wiggle &amp; Mass. Terdapat "
"juga dua tombol untuk menyalakan sensitivitas Pressure dan Tilt pada tablet "
"(untuk tablet gambar)."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:101
msgid "Width &amp; Thinning"
msgstr "Width &amp; Thinning"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:102
msgid ""
"This pair of options control the <firstterm>width</firstterm> of your pen. "
"The width can vary from 1 to 100 and (by default) is measured in units "
"relative to the size of your editing window, but independent of zoom. This "
"makes sense, because the natural “unit of measure” in calligraphy is the "
"range of your hand's movement, and it is therefore convenient to have the "
"width of your pen nib in constant ratio to the size of your “drawing board” "
"and not in some real units which would make it depend on zoom. This behavior "
"is optional though, so it can be changed for those who would prefer absolute "
"units regardless of zoom. To switch to this mode, use the checkbox on the "
"tool's Preferences page (you can open it by double-clicking the tool button)."
msgstr ""
"Sepasang pilihan ini mengatur <firstterm>width</firstterm> (lebar) dari pena "
"anda. Lebar ini bisa bervariasi dari 1 hingga 100 (standar) dan ukurannya "
"dalam satuan relatif terhadap besar dari jendela editan anda, tetapi "
"independen terhadap zum. Ini dikarenakan “satuan dari pengukuran“ dalam "
"kaligrafi adalah daerah pergerakan tangan anda, dengan demikian, akan lebih "
"baik jika lebar ujung pena anda rasionya konstan terhadap ukuran dari “papan "
"gambar“ dan bukannya ukuran nyata yang berpatokan pada zum. Meskipun begitu, "
"perlakuan ini adalah opsional, jadi bisa saja dirubah untuk mereka yang "
"lebih memilih berpatokan pada zum. Untuk berpindah ke mode ini, gunakan "
"kotak centang pada halaman Preferences tool yang bersangkutan (buka dengan "
"klik ganda pada tombol tool)."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:111
msgid ""
"Since pen width is changed often, you can adjust it without going to the "
"toolbar, using the <keycap>left</keycap> and <keycap>right</keycap> arrow "
"keys or with a tablet that supports the pressure sensitivity function. The "
"best thing about these keys is that they work while you are drawing, so you "
"can change the width of your pen gradually in the middle of the stroke:"
msgstr ""
"Dikarenakan lebar pena sering berubah, anda bisa mengaturnya tanpa harus "
"lewat toolbar, dengan tombol panah <keycap>kiri</keycap> dan panah "
"<keycap>kanan</keycap> atau lewat tablet yang mendukung fungsi pressure "
"sensitivity. Hal paling bagus dari tombol ini adalah mereka bisa digunakan "
"saat bekerja, jadi anda bisa merubah lebar pena anda saat anda membuat "
"sapuan:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:124
msgid ""
"Pen width may also depend on the velocity, as controlled by the "
"<firstterm>thinning</firstterm> parameter. This parameter can take values "
"from -100 to 100; zero means the width is independent of velocity, positive "
"values make faster strokes thinner, negative values make faster strokes "
"broader. The default of 10 means moderate thinning of fast strokes. Here are "
"a few examples, all drawn with width=20 and angle=90:"
msgstr ""
"Lebar pena juga bisa bergantung pada velositas, yang dikontrol oleh "
"parameter <firstterm>thinning</firstterm> (perampingan). Parameter ini bisa "
"bernilai -100 sampai dengan 100; nol berarti lebarnya akan tetap pada "
"velositas, positif membuat semakin cepat sapuan dilakukan hasilnya semakin "
"ramping, negatif akan membuatnya lebih lebar. Nilai bawaannya yaitu 10 "
"berarti sedikit perampingan pada sapuan cepat. Berikut adalah beberapa "
"contoh, semua digambar dengan lebar=20 dan sudut=90:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:137
msgid ""
"For fun, set Width and Thinning both to 100 (maximum) and draw with jerky "
"movements to get strangely naturalistic, neuron-like shapes:"
msgstr ""
"Untuk bersenang-senang, aturlah Width (lebar) dan Thinning (perampingan) "
"masing masing 100 (maksimum) dan cobalah menggambar dengan gerakan mengocok "
"untuk mendapatkan bentuk serupa neuron yang tampak aneh tapi terlihat "
"natural:"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:150
msgid "Angle &amp; Fixation"
msgstr "Sudut &amp; Fixation"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:151
msgid ""
"After width, <firstterm>angle</firstterm> is the most important calligraphy "
"parameter. It is the angle of your pen in degrees, changing from 0 "
"(horizontal) to 90 (vertical counterclockwise) or to -90 (vertical "
"clockwise). Note that if you turn tilt sensitivity on for a tablet, the "
"angle parameter is greyed out and the angle is determined by the tilt of the "
"pen."
msgstr ""
"Setelah lebar, <firstterm>angle</firstterm> (sudut) adalah parameter "
"kaligrafi yang penting. Itu adalah sudut dari pena anda dalam derajat, "
"berubah dari 0 (horisontal) ke 90 (vertikal berlawanan jarum jam) atau dari "
"-90 (vertikal searah jarum jam). Ingat bahwa jika anda menyalakan fitur tilt "
"sensitivity pada tablet, parameter sudut tidak diperdulikan dan semuanya "
"berdasarkan pena pada tablet anda."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:164
#, fuzzy
msgid ""
"Each traditional calligraphy style has its own prevalent pen angle. For "
"example, the Uncial hand uses the angle of 25 degrees. More complex hands "
"and more experienced calligraphers will often vary the angle while drawing, "
"and Inkscape makes this possible by pressing <keycap>up</keycap> and "
"<keycap>down</keycap> arrow keys or with a tablet that supports the tilt "
"sensitivity feature. For beginning calligraphy lessons, however, keeping the "
"angle constant will work best. Here are examples of strokes drawn at "
"different angles (fixation = 100):"
msgstr ""
"Setiap gaya kaligrafi tradisional memiliki sudut yang bermacam-macam. "
"Sebagai contoh, Unical hand menggunakan 25 derajat. Semakin kompleks handnya "
"dan semakin berpengalaman kaligrafernya akan membuat sudutnya semakin "
"beragam, dan Inkscape membuat ini mungkin dilakukan dengan menekan tombol "
"panah <keycap>atas</keycap> dan <keycap>bawah</keycap> atau dengan tablet "
"yang mendukung fitur tilt sensitivity. Untuk awal pembelajaran, sebaiknya "
"menggunakan sudut yang konstan. Berikut adalah beberapa contoh sapuan yang "
"digambar dengan sudut berbeda (fixation = 100):"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:178
msgid ""
"As you can see, the stroke is at its thinnest when it is drawn parallel to "
"its angle, and at its broadest when drawn perpendicular. Positive angles are "
"the most natural and traditional for right-handed calligraphy."
msgstr ""
"Seperti yang bisa anda lihat, sapuan paling ramping adalah jika digambar "
"paralel terhadap sudutnya, dan lebar jika sebaliknya. Sudut positif adalah "
"yang paling natural dan tradisional untuk kaligrafi tangan kanan."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:182
#, fuzzy
msgid ""
"The level of contrast between the thinnest and the thickest is controlled by "
"the <firstterm>fixation</firstterm> parameter. The value of 100 means that "
"the angle is always constant, as set in the Angle field. Decreasing fixation "
"lets the pen turn a little against the direction of the stroke. With "
"fixation=0, pen rotates freely to be always perpendicular to the stroke, and "
"Angle has no effect anymore:"
msgstr ""
"Tingkatan dari kontras antara yang paling ramping dan paling tebal diatur "
"oleh parameter <firstterm>fixation</firstterm>. Nilai 1 berarti sudutnya "
"selalu konstan, dan ditata pada daerah Angle (sudut). Menurunkan fixation "
"membuat pena berbalik lebih sedikit terhadap arah sapuan. Dengan fixation=0, "
"pena akan berbalik lebih bebas dan selalu searah dengan sapuannya, dan Angle "
"(sudut) tidak ada efeknya:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:195
msgid ""
"Typographically speaking, maximum fixation and therefore maximum stroke "
"width contrast (above left) are the features of antique serif typefaces, "
"such as Times or Bodoni (because these typefaces were historically an "
"imitation of fixed-pen calligraphy). Zero fixation and zero width contrast "
"(above right), on the other hand, suggest modern sans serif typefaces such "
"as Helvetica."
msgstr ""
"Mudahnya, fixation maksimum dan stroke width (lebar) maksimum (kiri atas) "
"adalah fitur dari typefaces antique serif, seperti Times atau Bodoni "
"(dikarenakan sejarahnya mereka adalah imitasi dari kaligrafi pena tetap). "
"Fixation nol dan kontras lebar nol (kanan atas), sebaliknya, lebih berupa "
"sans serif moderen seperti Helvatica."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:203
msgid "Tremor"
msgstr "Tremor"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:204
#, fuzzy
msgid ""
"<firstterm>Tremor</firstterm> is intended to give a more natural look to the "
"calligraphy strokes. Tremor is adjustable in the Controls bar with values "
"ranging from 0 to 100. It will affect your strokes producing anything from "
"slight unevenness to wild blotches and splotches. This significantly expands "
"the creative range of the tool."
msgstr ""
"<firstterm>Tremor</firstterm> dimaksudnya untuk memberikan kesan lebih "
"natural pada sapuan kaligrafi. Tremor bisa diatur pada Controls bar dengan "
"nilai bervariasi dari 0.0 sampai 1.0. Ia akan memberikan efek getar pada "
"sapuan sehingga lebih liar atau sedikit kasar. Ini akan membuat daerah "
"kreatif lebih bervariasi."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:219
msgid "Wiggle &amp; Mass"
msgstr "Wiggle &amp; Mass"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:220
msgid ""
"Unlike width and angle, these two last parameters define how the tool "
"“feels” rather than affect its visual output. So there won't be any "
"illustrations in this section; instead just try them yourself to get a "
"better idea."
msgstr ""
"Tidak seperti lebar dan sudut, dua parameter terakhir ini mendefinisikan "
"bagaimana tool tersebut “lebih berasa“. Dengan demikian, tidak akan ada "
"ilustrasi pada bagian ini; cobalah sendiri untuk memahami."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:225
msgid ""
"<firstterm>Wiggle</firstterm> is the resistance of the paper to the movement "
"of the pen. The default is at minimum (0), and increasing this parameter "
"makes paper “slippery”: if the mass is big, the pen tends to run away on "
"sharp turns; if the mass is zero, high wiggle makes the pen to wiggle wildly."
msgstr ""
"<firstterm>Wiggle</firstterm> adalah resistan kertas terhadap pergerakan "
"pena. Bawaannya adalah minimum (0), dan menaikkan parameter ini membuat "
"kertas semakin “licin“: jika massnya besar, pena akan lebih sering selip "
"pada tikungan tajam; jika mass nol, wiggle yang tinggi akan membuat pena "
"bergerak lebih liar."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:230
msgid ""
"In physics, <firstterm>mass</firstterm> is what causes inertia; the higher "
"the mass of the Inkscape calligraphy tool, the more it lags behind your "
"mouse pointer and the more it smoothes out sharp turns and quick jerks in "
"your stroke. By default this value is quite small (2) so that the tool is "
"fast and responsive, but you can increase mass to get slower and smoother "
"pen."
msgstr ""
"Dalam fisika, <firstterm>mass</firstterm> adalah penyebab inertia; semakin "
"besar mass dari tool tersebut, semakin lambar responnya terhadap tetikus dan "
"semakin halus tikungan dan sapuan kasar yang anda buat. Nilai bawaanya cukup "
"kecil (2) sehingga tool lebih cepat dan responsif, tapi tentu anda bisa "
"menaikkannya untuk mendapatkan pen yang lebih halus dan lambat."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:238
msgid "Calligraphy examples"
msgstr "Contoh kaligrafi"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:239
msgid ""
"Now that you know the basic capabilities of the tool, you can try to produce "
"some real calligraphy. If you are new to this art, get yourself a good "
"calligraphy book and study it with Inkscape. This section will show you just "
"a few simple examples."
msgstr ""
"Kini anda telah mengetahui kemampuan dasar dari tool tersebut, anda bisa "
"mencoba membuat kaligrafi. Jika anda baru terhadap seni ini, carilah buku "
"kaligrafi yang bagus dan pelajarilah dengan Inkscape. Bagian ini akan "
"menampilkan beberapa contoh sederhana."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:244
msgid ""
"First of all, to do letters, you need to create a pair of rulers to guide "
"you. If you're going to write in a slanted or cursive hand, add some slanted "
"guides across the two rulers as well, for example:"
msgstr ""
"Pertama, anda perlu membuat sepasang penggaris sebagai penunjuk. Jika anda "
"akan menulis dengan gaya tulisan indah misalnya, tambahkan beberapa guide "
"slanted diantara dua penggaris itu, contohnya:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:255
msgid ""
"Then zoom in so that the height between the rulers corresponds to your most "
"natural hand movement range, adjust width and angle, and off you go!"
msgstr ""
"Kemudian zum sehingga tinggi diantara penggaris tersebut sesuai dengan "
"daerah pergerakan tangan natural, atur lebar dan sudutnya, dan silahkan!"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:259
#, fuzzy
msgid ""
"Probably the first thing you would do as a beginner calligrapher is practice "
"the basic elements of letters — vertical and horizontal stems, round "
"strokes, slanted stems. Here are some letter elements for the Uncial hand:"
msgstr ""
"Kemungkinan hal pertama yang anda lakukan sebagai kaligrafer pemula adalah "
"berlatih elemen dasar huruf - stem vertikal dan horisontal, sapuan lengkung, "
"batang slanted. Berikut adalah beberapa elemen dari Unical hand:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:271
msgid "Several useful tips:"
msgstr "Beberapa tips berguna:"

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:276
#, fuzzy
msgid ""
"If your hand is comfortable on the tablet, don't move it. Instead, scroll "
"the canvas (<keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap>arrow</keycap></keycombo> keys) with your left hand after "
"finishing each letter."
msgstr ""
"Jika tangan anda sudah nyaman pada tablet, jangan gerakkan lagi. Gulung saja "
"kanvasnya (<keycap>Ctrl+panah</keycap>) dengan tangan kiri anda setiap "
"menyelesaikan tiap huruf."

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:283
#, fuzzy
msgid ""
"If your last stroke is bad, just undo it (<keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>Z</keycap></keycombo>). However, if its "
"shape is good but the position or size are slightly off, it's better to "
"switch to Selector temporarily (<keycap>Space</keycap>) and nudge/scale/"
"rotate it as needed (using <mousebutton>mouse</mousebutton> or keys), then "
"press <keycap>Space</keycap> again to return to Calligraphy tool."
msgstr ""
"Jika sapuan terakhir anda jelek, undo saja (<keycap>Ctrl+Z</keycap>). "
"Meskipun demikian, jika bentuknya bagus tetapi posisi atau ukurannya salah, "
"akan lebih baik untuk pindah ke Selector (<keycap>Spasi</keycap>) dan atur "
"ulang dengan menggunakan tetikus atau tombol, kemudian tekan lagi "
"<keycap>Spasi</keycap> untuk kembali ke tool Calligraphy."

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:292
msgid ""
"Having done a word, switch to Selector again to adjust stem uniformity and "
"letterspacing. Don't overdo this, however; good calligraphy must retain "
"somewhat irregular handwritten look. Resist the temptation to copy over "
"letters and letter elements; each stroke must be original."
msgstr ""
"Setelah menyelesaikan sehuruf, berpindahlah ke Selector lagi dan atur "
"keseragaman batangnya dan jarak perhurufnya. Jangan berlebihan; kaligrafi "
"yang baik harus memiliki tampilan yang menyerupai tulisan tangan. Kalau bisa "
"jangan menggandakan kemudian menempel huruf atau elemen huruf; setiap sapuan "
"haruslah asli."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:299
msgid "And here are some complete lettering examples:"
msgstr "Dan berikut adalah beberapa contoh:"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:311
msgid "Conclusion"
msgstr "Akhir"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:312
msgid ""
"Calligraphy is not only fun; it's a deeply spiritual art that may transform "
"your outlook on everything you do and see. Inkscape's calligraphy tool can "
"only serve as a modest introduction. And yet it is very nice to play with "
"and may be useful in real design. Enjoy!"
msgstr ""
"Kaligrafi bukan hanya untuk bersenang-senang; Ia adalah seni spiritual yang "
"dalam dan bisa merubah cara pandang anda. Tool kaligrafi inkscape hanyalah "
"perkenalan awal. Meskipun begitu, ia sangat bagus untuk bermain dan tetap "
"berguna pada desain nyata. Selamat menikmati!"

#. (itstool) path: Work/format
#: calligraphy-f01.svg:48 calligraphy-f02.svg:48 calligraphy-f03.svg:48
#: calligraphy-f04.svg:80 calligraphy-f05.svg:48 calligraphy-f06.svg:64
#: calligraphy-f07.svg:49 calligraphy-f08.svg:48 calligraphy-f09.svg:48
#: calligraphy-f10.svg:48
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: calligraphy-f01.svg:69
#, no-wrap
msgid "width=1, growing....                       reaching 47, decreasing...                                  back to 0"
msgstr "lebar=1,  membesar....                       menuju 47, mengecil...                                  kembali ke 0"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:69
#, no-wrap
msgid "thinning = 0 (uniform width) "
msgstr "perampingan = 0 (lebar seragam)"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:80
#, no-wrap
msgid "thinning = 10"
msgstr "perampingan = 10"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:91
#, no-wrap
msgid "thinning = 40"
msgstr "perampingan = 40"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:102
#, fuzzy, no-wrap
msgid "thinning = −20"
msgstr "perampingan = -20"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:113
#, fuzzy, no-wrap
msgid "thinning = −60"
msgstr "perampingan = -60"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:120
#, no-wrap
msgid "angle = 90 deg"
msgstr "sudut = 90 derajat"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:131
#, no-wrap
msgid "angle = 30 (default)"
msgstr "sudut = 30 (bawaan)"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:142 calligraphy-f05.svg:102
#, no-wrap
msgid "angle = 0"
msgstr "sudut = 0"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:153
#, fuzzy, no-wrap
msgid "angle = −90 deg"
msgstr "sudut = 90 derajat"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:69 calligraphy-f06.svg:85 calligraphy-f06.svg:101
#: calligraphy-f06.svg:117
#, no-wrap
msgid "angle = 30"
msgstr "sudut = 30"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:80
#, no-wrap
msgid "angle = 60"
msgstr "sudut = 60"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:91
#, no-wrap
msgid "angle = 90"
msgstr "sudut = 90"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:113
#, no-wrap
msgid "angle = 15"
msgstr "sudut = 15"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:124
#, fuzzy, no-wrap
msgid "angle = −45"
msgstr "sudut = -45"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:90
#, no-wrap
msgid "fixation = 100"
msgstr "fixation = 100"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:106
#, no-wrap
msgid "fixation = 80"
msgstr "fixation = 80"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:122
#, no-wrap
msgid "fixation = 0"
msgstr "fixation = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:73
#, no-wrap
msgid "slow"
msgstr "pelan"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:84
#, no-wrap
msgid "medium"
msgstr "sedang"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:95
#, no-wrap
msgid "fast"
msgstr "cepat"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:179
#, no-wrap
msgid "tremor = 0"
msgstr "tremor = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:190
#, no-wrap
msgid "tremor = 10"
msgstr "tremor = 10"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:201
#, no-wrap
msgid "tremor = 30"
msgstr "tremor = 30"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:212
#, no-wrap
msgid "tremor = 50"
msgstr "tremor = 50"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:223
#, no-wrap
msgid "tremor = 70"
msgstr "tremor = 70"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:234
#, no-wrap
msgid "tremor = 90"
msgstr "tremor = 90"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:245
#, no-wrap
msgid "tremor = 20"
msgstr "tremor = 20"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:256
#, no-wrap
msgid "tremor = 40"
msgstr "tremor = 40"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:267
#, no-wrap
msgid "tremor = 60"
msgstr "tremor = 60"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:278
#, no-wrap
msgid "tremor = 80"
msgstr "tremor = 80"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:289
#, no-wrap
msgid "tremor = 100"
msgstr "tremor = 100"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:117
#, no-wrap
msgid "Uncial hand"
msgstr "Uncial hand"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:128
#, no-wrap
msgid "Carolingian hand"
msgstr "Carolingian hand"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:139
#, no-wrap
msgid "Gothic hand"
msgstr "Gothic hand"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:150
#, no-wrap
msgid "Bâtarde hand"
msgstr "Bâtarde hand"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:175
#, no-wrap
msgid "Flourished Italic hand"
msgstr "Flourished Italic hand"

#~ msgid "Western or Roman"
#~ msgstr "Gaya Barat atau Roman"

#~ msgid "Arabic"
#~ msgstr "Arabik"

#~ msgid ""
#~ "This tutorial focuses mainly on Western calligraphy, as the other two "
#~ "styles tend to use a brush (instead of a pen with nib), which is not how "
#~ "our Calligraphy tool currently functions."
#~ msgstr ""
#~ "Tutorial kali ini berfokus pada kaligrafi Gaya Barat, dikarenakan dua "
#~ "yang lain biasanya harus menggunakan kuas (bukan pena), yang mana "
#~ "bukanlah bagaimana Calligraphy Tool saat ini bekerja."

#~ msgid "angle = -90 deg"
#~ msgstr "sudut = -90 derajat"

#~ msgid ""
#~ "bulia byak, buliabyak@users.sf.net and josh andler, scislac@users.sf.net"
#~ msgstr ""
#~ "bulia byak, buliabyak@users.sf.net dan josh andler, scislac@users.sf.net"
