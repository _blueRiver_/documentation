# Translators:
# Somogyvári Róbert <cr04ch at gmail>, 2009.
# Arpad Biro <biro.arpad gmail>, 2009.
# Balázs Meskó <meskobalazs@gmail.com>, 2018
# Gyuris Gellért <bubu@ujevangelizacio.hu>, 2018-2021.
#
msgid ""
msgstr ""
"Project-Id-Version: Inkscape tutorial: Calligraphy\n"
"POT-Creation-Date: 2021-10-11 17:29+0200\n"
"PO-Revision-Date: 2021-01-19 22:33+0100\n"
"Last-Translator: Gyuris Gellért <bubu@ujevangelizacio.hu>, 2020\n"
"Language-Team: Hungarian (https://www.transifex.com/fsf-hu/teams/77907/hu/)\n"
"Language: hu\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#. Put one translator per line, in the form NAME <EMAIL>, YEAR1, YEAR2
msgctxt "_"
msgid "translator-credits"
msgstr ""
"Somogyvári Róbert <cr04ch at gmail>, 2008\n"
"Gyuris Gellért <bubu@ujevangelizacio.hu>, 2018, 2019, 2020"

#. (itstool) path: articleinfo/title
#: tutorial-calligraphy.xml:6
msgid "Calligraphy"
msgstr "Művészi toll"

#. (itstool) path: articleinfo/subtitle
#: tutorial-calligraphy.xml:7
msgid "Tutorial"
msgstr "Ismertető"

#. (itstool) path: abstract/para
#: tutorial-calligraphy.xml:11
msgid ""
"One of the many great tools available in Inkscape is the Calligraphy tool. "
"This tutorial will help you become acquainted with how that tool works, as "
"well as demonstrate some basic techniques of the art of Calligraphy."
msgstr ""
"Az Inkscape-ben található remek eszközök egyike a Művészi toll. Most "
"megismerheti a működését, és bemutatunk néhány alapvető kalligrafikus "
"technikát is."

#. (itstool) path: abstract/para
#: tutorial-calligraphy.xml:15
msgid ""
"Use <keycombo><keycap function=\"control\">Ctrl</keycap><keycap>arrows</"
"keycap></keycombo>, <mousebutton role=\"mouse-wheel\">mouse wheel</"
"mousebutton>, or <mousebutton role=\"middle-button-drag\">middle button "
"drag</mousebutton> to scroll the page down. For basics of object creation, "
"selection, and transformation, see the Basic tutorial in "
"<menuchoice><guimenu>Help</guimenu><guimenuitem>Tutorials</guimenuitem></"
"menuchoice>."
msgstr ""
"A <keycombo><keycap function=\"control\">Ctrl</keycap><keycap>nyíl</keycap></"
"keycombo> billentyű, az <mousebutton role=\"mouse-wheel\">egérgörgő</"
"mousebutton> vagy az <mousebutton  role=\"middle-button-drag\">egér középső "
"gombbal való húzása</mousebutton> segít a lapozásban. Ha az objektumkészítés "
"alapjairól, a kijelölésről vagy az átalakításról szeretne olvasni, "
"javasoljuk a <menuchoice><guimenu>Súgó</guimenu><guimenuitem>Ismertetők</"
"guimenuitem></menuchoice> menüből a Bevezetést."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:23
msgid "History and Styles"
msgstr "A kalligráfia története és stílusai"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:24
msgid ""
"Going by the dictionary definition, <firstterm>calligraphy</firstterm> means "
"“beautiful writing” or “fair or elegant penmanship”. Essentially, "
"calligraphy is the art of making beautiful or elegant handwriting. It may "
"sound intimidating, but with a little practice, anyone can master the basics "
"of this art."
msgstr ""
"A <firstterm>kalligráfia</firstterm> a szótár meghatározása szerint "
"„szépírás” vagy „szabályos és tetszetős betűvetés”. Lényegében a kalligráfia "
"a szép vagy elegáns kézírás művészete. Ez riasztóan hangozhat, de egy kis "
"gyakorlással bárki elsajátíthatja ennek a művészetnek az alapjait."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:29
#, fuzzy
msgid ""
"Up until roughly 1440 AD, before the printing press was around, calligraphy "
"was the way books and other publications were made. A scribe had to "
"handwrite every individual copy of every book or publication. Perhaps the "
"most common place where the average person will run across calligraphy today "
"is on wedding invitations. There are three main styles of calligraphy:"
msgstr ""
"A kalligráfia legkorábbi formái a barlangrajzokból eredeztethetők. "
"Körülbelül 1440-ig, még mielőtt a könyvnyomtatás elterjedt volna, a könyvek "
"és egyéb iratok kalligráfiával készültek. Az írástudók minden könyv minden "
"egyes példányát kézírással készítették el. A kézíráshoz madártollat "
"használtak, ezzel vitték fel a tintát valamilyen anyagra, például pergamenre "
"vagy velinpapírra. A betűk stílusa koronként változott, például rusztika, "
"karoling, gótikus stb. Manapság a hétköznapi életben talán az esküvői "
"meghívókon találkozhatunk leginkább kalligrafikus írással."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:32
msgid "There are three main styles of calligraphy:"
msgstr "A kalligráfia három fő stílusa a következő:"

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:37
msgid ""
"Western or Roman: The preserved texts are mainly written with a quill and "
"ink onto materials such as parchment or vellum."
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:42
msgid ""
"Arabic, including other languages in Arabic script, executed with a reed pen "
"and ink on paper. (This style has produced by far the largest bulk of "
"preserved texts before 1440.)"
msgstr ""

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:47
#, fuzzy
msgid "Chinese or Oriental, executed with a brush."
msgstr "kínai vagy keleti."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:52
msgid ""
"The three styles have influenced each other on different occasions. Western "
"lettering styles used throughout the ages include Rustic, Carolingian, "
"Blackletter, etc."
msgstr ""

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:55
msgid ""
"One great advantage that we have over the scribes of the past is the "
"<guimenuitem>Undo</guimenuitem> command: If you make a mistake, the entire "
"page is not ruined. Inkscape's Calligraphy tool also enables some techniques "
"which would not be possible with a traditional pen-and-ink."
msgstr ""
"A régi írástudókkal szemben nekünk van egy nagy előnyünk, mégpedig a "
"<guimenuitem>Visszavonás</guimenuitem> parancs: egy rossz mozdulat sose "
"teszi tönkre az egész lapot. Az Inkscape Művészi tolla néhány olyan "
"technikára is lehetőséget ad, amelyre a hagyományos toll és tinta nem."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:62
msgid "Hardware"
msgstr "Hardver"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:63
msgid ""
"You will get the best results if you use a <firstterm>tablet and pen</"
"firstterm> (e.g. Wacom). Thanks to the flexibility of our tool, even those "
"with only a mouse can do some fairly intricate calligraphy, though there "
"will be some difficulty producing fast sweeping strokes."
msgstr ""
"A legjobb eredmény <firstterm>digitális tábla</firstterm> (pl. Wacom) "
"használatával érhető el. De hála a Művészi toll rugalmasságának, pusztán egy "
"egérrel is egész bonyolult kalligráfiák rajzolhatók, habár nagy lendületet "
"igénylő vonásokat húzni így meglehetősen nehéz."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:68
msgid ""
"Inkscape is able to utilize <firstterm>pressure sensitivity</firstterm> and "
"<firstterm> tilt sensitivity</firstterm> of a tablet pen that supports these "
"features. The sensitivity functions are disabled by default because they "
"require configuration. Also, keep in mind that calligraphy with a quill or "
"pen with nib are also not very sensitive to pressure, unlike a brush."
msgstr ""
"Az Inkscape fel tudja dolgozni a <firstterm>nyomáserősség-</firstterm> és "
"<firstterm>dőlésszög-</firstterm>információkat, ha a digitális tábla képes "
"ilyen adatokat szolgáltatni. A programban ezek az érzékenységi funkciók "
"kezdetben ki vannak kapcsolva, mert első használat előtt beállítást "
"igényelnek. Azt se feledje, hogy a kalligráfia nem annyira érzékeny a "
"nyomásra tollheggyel, mint ecsettel."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:74
msgid ""
"If you have a tablet and would like to utilize the sensitivity features, you "
"will need to configure your device. This configuration will only need to be "
"performed once and the settings are saved. To enable this support you must "
"have the tablet plugged in prior to starting Inkscape and then proceed to "
"open the <guimenuitem>Input Devices...</guimenuitem> dialog through the "
"<guimenu>Edit</guimenu> menu. With this dialog open you can choose the "
"preferred device and settings for your tablet pen. Lastly, after choosing "
"those settings, switch to the Calligraphy tool and toggle the toolbar "
"buttons for pressure and tilt. From now on, Inkscape will remember those "
"settings on startup."
msgstr ""
"Ha van digitális táblája, és ki szeretné használni ezeket az érzékenységi "
"információk adta lehetőségeket, akkor tehát megfelelően be kell állítania az "
"eszközét. Ezt a beállítást csak egyszer kell elvégezni, a program elmenti a "
"beállításokat. Először, még az Inkscape indítása előtt csatlakoztassa a "
"digitális táblát, majd nyissa meg a <guimenu>Szerkesztés</guimenu> menüből a "
"<guimenuitem>Beviteli eszközök…</guimenuitem> párbeszédablakot. Itt ki tudja "
"választani, és be is állíthatja az eszközét. Végül, miután mindent "
"beállított, váltson a Művészi toll eszközre, és az Eszközvezérlősávon a "
"megfelelő gombokkal kapcsolja be a nyomás- és dőlésérzékelést. Az Inkscape "
"ezután már emlékezni fog a beállításokra."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:83
msgid ""
"The Inkscape calligraphy pen can be sensitive to the <firstterm>velocity</"
"firstterm> of the stroke (see “Thinning” below), so if you are using a "
"mouse, you'll probably want to zero this parameter."
msgstr ""
"A Művészi toll a rajzolás <firstterm>sebesség</firstterm>ét is érzékelni "
"tudja (további információ a „Keskenyítés”-nél később), ezért ha egeret "
"használ, akkor ez a paraméter valószínűleg nullára állítva felel majd meg "
"Önnek."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:89
msgid "Calligraphy Tool Options"
msgstr "A Művészi toll beállításai"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:90
msgid ""
"Switch to the Calligraphy tool by pressing <keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>F6</keycap></keycombo>, pressing the "
"<keycap>C</keycap> key, or by clicking on its toolbar button. On the top "
"toolbar, you will notice there are 8 options: <guilabel>Width</guilabel> "
"&amp; <guilabel>Thinning</guilabel>; <guilabel>Angle</guilabel> &amp; "
"<guilabel>Fixation</guilabel>; <guilabel>Caps</guilabel>; <guilabel>Tremor</"
"guilabel>, <guilabel>Wiggle</guilabel> &amp; <guilabel>Mass</guilabel>. "
"There are also two buttons to toggle tablet Pressure and Tilt sensitivity on "
"and off (for drawing tablets)."
msgstr ""
"Váltson a Művészi toll eszközre a <keycombo><keycap function=\"control"
"\">Ctrl</keycap><keycap>F6</keycap></keycombo> vagy a <keycap>C</keycap> "
"megnyomásával, esetleg az ikonjára kattintva az Eszköztáron. Az "
"Eszközvezérlősávon 8 beállítást láthat: <guilabel>Szélesség</guilabel> és "
"<guilabel>Keskenyítés</guilabel>; <guilabel>Szög</guilabel> és "
"<guilabel>Rögzítettség</guilabel>; <guilabel>Vonalvég</guilabel>; "
"<guilabel>Remegés</guilabel>; <guilabel>Tekeredés</guilabel> és "
"<guilabel>Tömeg</guilabel>. Szintén itt található az a két gomb, melyekkel a "
"nyomás- és dőlésérzékelés be- és kikapcsolható (digitális táblákhoz)."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:101
msgid "Width &amp; Thinning"
msgstr "Szélesség és Keskenyítés"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:102
msgid ""
"This pair of options control the <firstterm>width</firstterm> of your pen. "
"The width can vary from 1 to 100 and (by default) is measured in units "
"relative to the size of your editing window, but independent of zoom. This "
"makes sense, because the natural “unit of measure” in calligraphy is the "
"range of your hand's movement, and it is therefore convenient to have the "
"width of your pen nib in constant ratio to the size of your “drawing board” "
"and not in some real units which would make it depend on zoom. This behavior "
"is optional though, so it can be changed for those who would prefer absolute "
"units regardless of zoom. To switch to this mode, use the checkbox on the "
"tool's Preferences page (you can open it by double-clicking the tool button)."
msgstr ""
"Ez a két beállítás vezérli a tollhegy <firstterm>szélesség</firstterm>ét. A "
"szélesség értéke 1 és 100 között lehet, a mértékegysége pedig "
"(alapértelmezetten) a rajzablak méretétől függően változik, de a nagyítástól "
"független. Ez azért van, mert a természetes „mértékegység” a kalligráfiában "
"a kézmozgáshoz igazodik. Ez a módszer biztosítja, hogy a tollhegy szélessége "
"állandó arányban maradjon a „rajztáblával”, szemben egy tényleges "
"mértékegységgel, amely a tollhegyszélességet a nagyítástól tenné függővé. "
"Mindazonáltal ez a viselkedés nem kötelező, így meg is változtatható. Ha "
"valaki a nagyítástól eltekintve abszolút mértékegységet szeretne, ezt az "
"Inkscape-beállítások ablakban (megnyitható dupla kattintással az eszköz "
"gombján) tudathatja a programmal."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:111
msgid ""
"Since pen width is changed often, you can adjust it without going to the "
"toolbar, using the <keycap>left</keycap> and <keycap>right</keycap> arrow "
"keys or with a tablet that supports the pressure sensitivity function. The "
"best thing about these keys is that they work while you are drawing, so you "
"can change the width of your pen gradually in the middle of the stroke:"
msgstr ""
"Mivel a tollhegy szélességét gyakran szokás változtatni, erre az "
"Eszközvezérlősávon kívül más mód is van. Használhatja a <keycap>bal</keycap> "
"vagy a <keycap>jobb</keycap> kurzormozgató billentyűt, megfelelő digitális "
"táblával pedig a nyomásérzékenységet. Az a legjobb ezekben a billentyűkben, "
"hogy rajzolás közben is működnek, így fokozatosan változtathatja a tollhegy "
"szélességét egy tollvonás közbenső részén is:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:124
msgid ""
"Pen width may also depend on the velocity, as controlled by the "
"<firstterm>thinning</firstterm> parameter. This parameter can take values "
"from -100 to 100; zero means the width is independent of velocity, positive "
"values make faster strokes thinner, negative values make faster strokes "
"broader. The default of 10 means moderate thinning of fast strokes. Here are "
"a few examples, all drawn with width=20 and angle=90:"
msgstr ""
"A tollhegy szélessége a <firstterm>keskenyítés</firstterm> által a tollvonás "
"sebességétől is függ. Ennek a paraméternek −100 és 100 között lehet az "
"értéke; nulla esetén a szélesség független a sebességtől, pozitív érték "
"esetén a gyorsulás vékonyít, negatív értéknél vastagít. Az alapértelmezett "
"10 gyors tollvonásoknál mérsékelt vékonyítást okoz. Bemutatunk néhány "
"példát, mindegyiknél 20 volt a Szélesség értéke, a Szögé pedig 90:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:137
msgid ""
"For fun, set Width and Thinning both to 100 (maximum) and draw with jerky "
"movements to get strangely naturalistic, neuron-like shapes:"
msgstr ""
"Szórakozásképpen beállíthatja a Szélességet és a Keskenyítést is 100-ra (a "
"maximumra), és hirtelen mozdulatokkal rajzolva ilyen különösképpen "
"természetesnek tűnő, neuronszerű formákhoz juthat:"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:150
msgid "Angle &amp; Fixation"
msgstr "Szög és Rögzítettség"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:151
msgid ""
"After width, <firstterm>angle</firstterm> is the most important calligraphy "
"parameter. It is the angle of your pen in degrees, changing from 0 "
"(horizontal) to 90 (vertical counterclockwise) or to -90 (vertical "
"clockwise). Note that if you turn tilt sensitivity on for a tablet, the "
"angle parameter is greyed out and the angle is determined by the tilt of the "
"pen."
msgstr ""
"A kalligráfiában a tollszélesség mellett a <firstterm>szög</firstterm> is "
"nagyon fontos jellemző. Ez a toll szögét jelenti fokban, az értéke 0-tól "
"(vízszintes) 90-ig (függőleges, az óramutató járásával ellentétesen) vagy "
"−90-ig (függőleges, az óramutató járásával egyezően) változhat. Vegye "
"figyelembe, hogy ha egy digitális táblához bekapcsolta a dőlésszög-"
"érzékenységet, akkor a Szög paraméter nem változtatható, a szöget a toll "
"szöge határozza meg."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:164
msgid ""
"Each traditional calligraphy style has its own prevalent pen angle. For "
"example, the Uncial hand uses the angle of 25 degrees. More complex hands "
"and more experienced calligraphers will often vary the angle while drawing, "
"and Inkscape makes this possible by pressing <keycap>up</keycap> and "
"<keycap>down</keycap> arrow keys or with a tablet that supports the tilt "
"sensitivity feature. For beginning calligraphy lessons, however, keeping the "
"angle constant will work best. Here are examples of strokes drawn at "
"different angles (fixation = 100):"
msgstr ""
"Minden hagyományos kalligrafikus stílusra jellemző egy uralkodó dőlésszög. "
"Például az unciális írásban ez a szög 25°. Bonyolultabb kézírásoknál "
"gyakorlott kalligráfusok gyakran változtatják rajzolás közben a toll szögét, "
"és az Inkscape meg is adja erre a lehetőséget a <keycap>fel</keycap> és a "
"<keycap>le</keycap> billentyűkkel vagy a digitális tábla dőlésszög-"
"érzékenységét kihasználva. Kezdetben azért jobb lesz, ha az állandó szögnél "
"marad. Bemutatunk néhány, különböző szögekkel készült vonalat (Rögzítettség "
"= 100):"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:178
msgid ""
"As you can see, the stroke is at its thinnest when it is drawn parallel to "
"its angle, and at its broadest when drawn perpendicular. Positive angles are "
"the most natural and traditional for right-handed calligraphy."
msgstr ""
"Amint láthatja, a vonal ott a legvékonyabb, ahol a toll szögével "
"párhuzamosan fut, és ott a legvastagabb, ahol a toll szögére merőlegesen. A "
"pozitív szögek természetesebb hatást keltenek, és a jobbkezes kalligráfiában "
"hagyományosan ezek fordulnak elő."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:182
msgid ""
"The level of contrast between the thinnest and the thickest is controlled by "
"the <firstterm>fixation</firstterm> parameter. The value of 100 means that "
"the angle is always constant, as set in the Angle field. Decreasing fixation "
"lets the pen turn a little against the direction of the stroke. With "
"fixation=0, pen rotates freely to be always perpendicular to the stroke, and "
"Angle has no effect anymore:"
msgstr ""
"A lehetséges legvékonyabb és legvastagabb rész közötti különbség "
"szabályozható a <firstterm>rögzítettség</firstterm>gel. Ha ennek az értéke "
"100, akkor a szög mindig a „szög” paraméterrel beállított állandó érték. A "
"rögzítettség csökkentésével a toll kissé a tollvonás irányával ellentétesen "
"fordul el. Ha a rögzítettség 0, akkor a toll szabadon foroghat, és a "
"tollvonásra mindig merőleges lesz, a szög értékének ilyenkor megszűnik a "
"hatása:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:195
msgid ""
"Typographically speaking, maximum fixation and therefore maximum stroke "
"width contrast (above left) are the features of antique serif typefaces, "
"such as Times or Bodoni (because these typefaces were historically an "
"imitation of fixed-pen calligraphy). Zero fixation and zero width contrast "
"(above right), on the other hand, suggest modern sans serif typefaces such "
"as Helvetica."
msgstr ""
"Tipográfiai szempontból a legnagyobb rögzítettségből adódó legnagyobb "
"szélességeltérés (fent balra) az antik, talpas betűképekre jellemző, mint "
"például a Times vagy a Bodoni (mivel ezek történetileg a rögzített tollszögű "
"kalligráfia utánzásából jöttek létre). A nulla rögzítettség és nulla eltérés "
"(jobbra fent) pedig a modern, groteszk betűképekkel azonosítható, mint "
"például a Helvetica."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:203
msgid "Tremor"
msgstr "Remegés"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:204
msgid ""
"<firstterm>Tremor</firstterm> is intended to give a more natural look to the "
"calligraphy strokes. Tremor is adjustable in the Controls bar with values "
"ranging from 0 to 100. It will affect your strokes producing anything from "
"slight unevenness to wild blotches and splotches. This significantly expands "
"the creative range of the tool."
msgstr ""
"A <firstterm>remegés</firstterm> célja, hogy a kalligrafikus tollvonások még "
"természetesebbnek tűnjenek. A remegés mértéke az eszközvezérlősávon 0 és 100 "
"között szabályozható. A hatás a csekély egyenetlenségtől a durva foltokig és "
"pacákig terjed. Ez nagymértékben növeli az eszköz kreatív alkalmazhatóságát."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:219
msgid "Wiggle &amp; Mass"
msgstr "Tekeredés és Tömeg"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:220
msgid ""
"Unlike width and angle, these two last parameters define how the tool "
"“feels” rather than affect its visual output. So there won't be any "
"illustrations in this section; instead just try them yourself to get a "
"better idea."
msgstr ""
"A szélességgel és a szöggel szemben ez a két paraméter a látható hatás "
"helyett az eszköz „érzetkeltését” alakítja. Ezért nem is talál illusztrációt "
"ebben a részben; inkább próbálja ki, hogy jobban megértse."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:225
msgid ""
"<firstterm>Wiggle</firstterm> is the resistance of the paper to the movement "
"of the pen. The default is at minimum (0), and increasing this parameter "
"makes paper “slippery”: if the mass is big, the pen tends to run away on "
"sharp turns; if the mass is zero, high wiggle makes the pen to wiggle wildly."
msgstr ""
"A <firstterm>Tekeredés</firstterm> a papír tollra gyakorolt ellenállását "
"szabályozza. Alapértelmezett értéke egyben a lehető legkisebb (0), növelve a "
"papír „csúszósabb” lesz: nagy tömegnél a toll hajlamos lesz éles fordulóknál "
"túlcsúszni; magas tekeredés mellett nulla tömeg a toll tekergőzését vadabbá "
"teszi."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:230
msgid ""
"In physics, <firstterm>mass</firstterm> is what causes inertia; the higher "
"the mass of the Inkscape calligraphy tool, the more it lags behind your "
"mouse pointer and the more it smoothes out sharp turns and quick jerks in "
"your stroke. By default this value is quite small (2) so that the tool is "
"fast and responsive, but you can increase mass to get slower and smoother "
"pen."
msgstr ""
"A fizikában a <firstterm>tömeg</firstterm> a tehetetlenség oka; az Inkscape "
"Művészi tolla esetében a tömeget növelve a toll egyre inkább lemarad az "
"egérmutató mozgásáról, illetve egyre simábbak lesznek a tollvonások éles "
"fordulói és hirtelen megugrásai. Alapesetben a Tömeg nagyon kicsi (2), így "
"az eszköz gyors és érzékeny, de növelheti az értéket, ha lassabb és "
"egyenletesebb működést szeretne."

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:238
msgid "Calligraphy examples"
msgstr "Kalligráfia a gyakorlatban"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:239
msgid ""
"Now that you know the basic capabilities of the tool, you can try to produce "
"some real calligraphy. If you are new to this art, get yourself a good "
"calligraphy book and study it with Inkscape. This section will show you just "
"a few simple examples."
msgstr ""
"Most, hogy megismerte az eszköz alapvető képességeit, megpróbálhat "
"elkészíteni néhány valódi kalligráfiát. Ha még csak most kezdett ezzel a "
"művészettel foglalkozni, szerezzen egy jó könyvet a témáról, és tanuljon az "
"Inkscape segítségével. Ez a rész csak néhány egyszerű példát fog bemutatni:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:244
msgid ""
"First of all, to do letters, you need to create a pair of rulers to guide "
"you. If you're going to write in a slanted or cursive hand, add some slanted "
"guides across the two rulers as well, for example:"
msgstr ""
"Mindenekelőtt készítenie kell egy vezetővonalpárt, ami segít a betűk "
"elkészítésében. Ha döntött vagy dőlt betűs írásra gondol, akkor néhány ferde "
"vezetővonalra is szüksége lesz, keresztben a két vízszintessel, valahogy így:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:255
msgid ""
"Then zoom in so that the height between the rulers corresponds to your most "
"natural hand movement range, adjust width and angle, and off you go!"
msgstr ""
"Ezután a nagyítást állítsa olyanra, hogy a vezetők közötti magasság "
"megfeleljen a természetes kézmozgásának. Adja meg a Szélességet és a Szöget, "
"és már neki is foghat a rajzolásnak."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:259
msgid ""
"Probably the first thing you would do as a beginner calligrapher is practice "
"the basic elements of letters — vertical and horizontal stems, round "
"strokes, slanted stems. Here are some letter elements for the Uncial hand:"
msgstr ""
"Kezdő kalligráfusként az első dolga valószínűleg a betűk alapelemeinek a "
"begyakorlása lesz – vízszintes és függőleges szárak, lekerekített vonalak, "
"ferde szárak. Mutatunk néhány, az unciális íráshoz való betűelemet:"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:271
msgid "Several useful tips:"
msgstr "Hasznos tanácsok:"

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:276
msgid ""
"If your hand is comfortable on the tablet, don't move it. Instead, scroll "
"the canvas (<keycombo><keycap function=\"control\">Ctrl</"
"keycap><keycap>arrow</keycap></keycombo> keys) with your left hand after "
"finishing each letter."
msgstr ""
"Ha a digitális táblával megtalálta a kényelmes kézpozíciót, akkor egy-egy "
"betű elkészültekor ne ezt a kezét mozgassa, inkább a vásznat görgesse (a "
"<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>nyíl</keycap></"
"keycombo> billentyűkkel)."

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:283
msgid ""
"If your last stroke is bad, just undo it (<keycombo><keycap function="
"\"control\">Ctrl</keycap><keycap>Z</keycap></keycombo>). However, if its "
"shape is good but the position or size are slightly off, it's better to "
"switch to Selector temporarily (<keycap>Space</keycap>) and nudge/scale/"
"rotate it as needed (using <mousebutton>mouse</mousebutton> or keys), then "
"press <keycap>Space</keycap> again to return to Calligraphy tool."
msgstr ""
"Ha a legutóbbi tollvonása nem sikerült, akkor egyszerűen vonja vissza "
"(<keycombo><keycap function=\"control\">Ctrl</keycap><keycap>Z</keycap></"
"keycombo>). Viszont ha a vonás alakja jó, csak a pozíciója vagy a mérete "
"lett kissé elhibázott, akkor célszerűbb ideiglenesen a Kijelölő eszközre "
"váltva (<keycap>Szóköz</keycap>) a szükséges mértékben mozgatni, átméretezni "
"vagy forgatni (<mousebutton>egér</mousebutton>rel vagy billentyűkkel). "
"Ezután a Szóköz ismételt megnyomásával újra a Művészi tollat használhatja."

#. (itstool) path: listitem/para
#: tutorial-calligraphy.xml:292
msgid ""
"Having done a word, switch to Selector again to adjust stem uniformity and "
"letterspacing. Don't overdo this, however; good calligraphy must retain "
"somewhat irregular handwritten look. Resist the temptation to copy over "
"letters and letter elements; each stroke must be original."
msgstr ""
"Ha elkészült egy szó, váltson ismét a Kijelölő eszközre, és igazítsa "
"egyenletesre a betűszárakat és a betűközöket. De ne essen túlzásokba; a jó "
"kalligráfiának meg kell őriznie valamit az eredeti kézírások "
"szabálytalanságaiból. Álljon ellen a kísértésnek, hogy másolással jusson "
"újabb betűkhöz vagy betűrészekhez; az a jó, ha minden tollvonás egyedi."

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:299
msgid "And here are some complete lettering examples:"
msgstr "És végül álljon itt néhány példa kész betűkre:"

#. (itstool) path: sect1/title
#: tutorial-calligraphy.xml:311
msgid "Conclusion"
msgstr "Zárszó"

#. (itstool) path: sect1/para
#: tutorial-calligraphy.xml:312
msgid ""
"Calligraphy is not only fun; it's a deeply spiritual art that may transform "
"your outlook on everything you do and see. Inkscape's calligraphy tool can "
"only serve as a modest introduction. And yet it is very nice to play with "
"and may be useful in real design. Enjoy!"
msgstr ""
"A kalligráfia nem csupán szórakoztató; mélyen spirituális művészet, mely "
"formálhatja a szemléletmódját bármivel kapcsolatban, amit tesz vagy lát. Az "
"Inkscape Művészi tolla csak szerény bevezetést adhat ebbe a művészetbe. De "
"mégis nagyon jó vele eljátszani, és hasznára lehet a munkában is. Kellemes "
"időtöltést!"

#. (itstool) path: Work/format
#: calligraphy-f01.svg:48 calligraphy-f02.svg:48 calligraphy-f03.svg:48
#: calligraphy-f04.svg:80 calligraphy-f05.svg:48 calligraphy-f06.svg:64
#: calligraphy-f07.svg:49 calligraphy-f08.svg:48 calligraphy-f09.svg:48
#: calligraphy-f10.svg:48
msgid "image/svg+xml"
msgstr "image/svg+xml"

#. (itstool) path: text/tspan
#: calligraphy-f01.svg:69
#, no-wrap
msgid "width=1, growing....                       reaching 47, decreasing...                                  back to 0"
msgstr "szélesség=1, növelés…                    47-ig,        csökkentés…                                egészen 0-ig"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:69
#, no-wrap
msgid "thinning = 0 (uniform width) "
msgstr "keskenyítés = 0 (állandó szélesség)"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:80
#, no-wrap
msgid "thinning = 10"
msgstr "keskenyítés = 10"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:91
#, no-wrap
msgid "thinning = 40"
msgstr "keskenyítés = 40"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:102
#, no-wrap
msgid "thinning = −20"
msgstr "keskenyítés = −20"

#. (itstool) path: text/tspan
#: calligraphy-f02.svg:113
#, no-wrap
msgid "thinning = −60"
msgstr "keskenyítés = −60"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:120
#, no-wrap
msgid "angle = 90 deg"
msgstr "szög = 90°"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:131
#, no-wrap
msgid "angle = 30 (default)"
msgstr "szög = 30 (alapértelmezett)"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:142 calligraphy-f05.svg:102
#, no-wrap
msgid "angle = 0"
msgstr "szög = 0"

#. (itstool) path: text/tspan
#: calligraphy-f04.svg:153
#, no-wrap
msgid "angle = −90 deg"
msgstr "szög = −90°"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:69 calligraphy-f06.svg:85 calligraphy-f06.svg:101
#: calligraphy-f06.svg:117
#, no-wrap
msgid "angle = 30"
msgstr "szög = 30"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:80
#, no-wrap
msgid "angle = 60"
msgstr "szög = 60"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:91
#, no-wrap
msgid "angle = 90"
msgstr "szög = 90"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:113
#, no-wrap
msgid "angle = 15"
msgstr "szög = 15"

#. (itstool) path: text/tspan
#: calligraphy-f05.svg:124
#, no-wrap
msgid "angle = −45"
msgstr "szög = −45"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:90
#, no-wrap
msgid "fixation = 100"
msgstr "rögzítettség = 100"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:106
#, no-wrap
msgid "fixation = 80"
msgstr "rögzítettség = 80"

#. (itstool) path: text/tspan
#: calligraphy-f06.svg:122
#, no-wrap
msgid "fixation = 0"
msgstr "rögzítettség = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:73
#, no-wrap
msgid "slow"
msgstr "lassú"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:84
#, no-wrap
msgid "medium"
msgstr "közepes"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:95
#, no-wrap
msgid "fast"
msgstr "gyors"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:179
#, no-wrap
msgid "tremor = 0"
msgstr "remegés = 0"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:190
#, no-wrap
msgid "tremor = 10"
msgstr "remegés = 10"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:201
#, no-wrap
msgid "tremor = 30"
msgstr "remegés = 30"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:212
#, no-wrap
msgid "tremor = 50"
msgstr "remegés = 50"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:223
#, no-wrap
msgid "tremor = 70"
msgstr "remegés = 70"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:234
#, no-wrap
msgid "tremor = 90"
msgstr "remegés = 90"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:245
#, no-wrap
msgid "tremor = 20"
msgstr "remegés = 20"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:256
#, no-wrap
msgid "tremor = 40"
msgstr "remegés = 40"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:267
#, no-wrap
msgid "tremor = 60"
msgstr "remegés = 60"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:278
#, no-wrap
msgid "tremor = 80"
msgstr "remegés = 80"

#. (itstool) path: text/tspan
#: calligraphy-f07.svg:289
#, no-wrap
msgid "tremor = 100"
msgstr "remegés = 100"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:117
#, no-wrap
msgid "Uncial hand"
msgstr "Unciális írás"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:128
#, no-wrap
msgid "Carolingian hand"
msgstr "Karoling írás"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:139
#, no-wrap
msgid "Gothic hand"
msgstr "Gótikus írás"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:150
#, no-wrap
msgid "Bâtarde hand"
msgstr "Basztard írás"

#. (itstool) path: text/tspan
#: calligraphy-f10.svg:175
#, no-wrap
msgid "Flourished Italic hand"
msgstr "Cifrázott, dőlt betűs írás"

#~ msgid "Western or Roman"
#~ msgstr "nyugati vagy római"

#~ msgid "Arabic"
#~ msgstr "arab"

#~ msgid ""
#~ "This tutorial focuses mainly on Western calligraphy, as the other two "
#~ "styles tend to use a brush (instead of a pen with nib), which is not how "
#~ "our Calligraphy tool currently functions."
#~ msgstr ""
#~ "Ez az írás főleg a nyugati kalligráfiával foglalkozik, mivel a másik két "
#~ "stílus az ecsetet részesítette előnyben (a hegyezett tollal szemben), azt "
#~ "pedig másképpen kell használni, mint ahogy jelenleg a Művészi tollat "
#~ "lehet."
